﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TheVet.Api.Resources;
using TheVet.Core.Contracts.Services;
using TheVetBackEnd.Models;

namespace TheVetBackEnd.Controllers
{
    [Produces("application/json")]
    [Route("api/Pets")]
    public class PetsController : Controller
    {
        private readonly IPetService petService;
        private readonly IMapper mapper;

        public PetsController(IPetService service, IMapper mapper)
        {
            this.petService = service;
            this.mapper = mapper;
        }

        // GET: api/Pets
        [HttpGet]
        public IActionResult GetPet()
        {
            var result = petService.FindAllPets();
            if (result.Count() <= 0)
            {
                return NotFound(new Exception("Nenhum registro foi encontrado!"));
            }
            else
                return Ok(result);
        }

        //// GET: api/Pets/5
        [HttpGet("{id}")]
        public IActionResult GetPet([FromRoute] Guid id)
        {
            var pet = petService.FindPetById(id);

            if (pet == null)
            {
                return NotFound("Nenhum registro foi encontrado!");
            }

            return Ok(pet);
        }

        //// PUT: api/Pets/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPet([FromRoute] Guid id, [FromBody] PetResource petResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var petUpdate = this.mapper.Map<PetResource, Pet>(petResource);
            if (id != petResource.Code)
            {
                return BadRequest( "O código do Pet selecionado é diferente do parâmetro enviado na rota!");
            }
            try
            {
                await this.petService.UpdatePet(petUpdate);
                return Ok("Pet atualizado com sucesso!");
            }
            catch (Exception ex)
            {
                return NotFound(new Exception(string.Format("Erro ao atualizar o Pet! Erro: {0}", ex.Message)));
            }
        }

        // POST: api/Pets
        [HttpPost]
        public async Task<IActionResult> PostPet([FromBody] SavePetResource petResource)
        {
            if (petResource == null)
            {
                return BadRequest("Dados inválidos");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pet = this.mapper.Map<SavePetResource, Pet>(petResource);
            try
            {
                await this.petService.CreatePet(pet);
                return Ok("Pet cadastrado com sucesso!");
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("Erro ao cadastrar Pet! Erro: {0}", ex.Message));
            }
        }

        //// DELETE: api/Pets/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePet([FromRoute] Guid id)
        {
            try
            {
                await petService.Delete(id);
                return Ok("Pet removido com sucesso!");
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("Erro ao remover Pet. Erro: {0}", ex.Message));
            }
        }

        [HttpGet, Route("petswithuser")]
        public async Task<IActionResult> PetsWithUser([FromQuery][Required] Guid userId)
        {
            if (userId == Guid.Empty)
            {
                return BadRequest("Código do usuário inválido!");
            }

            var response = await this.petService.FindAllWithUser(userId);
            return Ok(response);
        }
    }
}
