﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheVet.Api.Resources;
using TheVet.Core.Contracts.Services;
using TheVetBackEnd.Models;

namespace TheVet.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public UserController(IUserService service, IMapper mapper)
        {
            this.userService = service;
            this.mapper = mapper;
        }


        [HttpPost]
        public async Task<IActionResult> PostUser([FromBody] SaveUserResource user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var mappedUser = this.mapper.Map<SaveUserResource, User>(user);
            try
            {
                var createdUser = await this.userService.CreateUser(mappedUser);
                var userResource = this.mapper.Map<User, UserResource>(createdUser);
                return Ok(userResource);
            }
            catch (Exception e)
            {
                return BadRequest(new {message = "Não foi possível cadastrar o usuário", error = e.Message});
            }
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody] UserLogin userLogin)
        {
            var returnedStatus = this.userService.ValidateLogin(userLogin.Username, userLogin.Password);
            if (returnedStatus.Success)
            {
                var user = returnedStatus.ReturnedUser;
                var userResource = this.mapper.Map<User, UserResource>(user);
                return Ok(userResource);
            }
            else
            {
                return BadRequest(new
                {
                    returnedStatus.Message
                });
            }
        }

        [HttpGet, Route("userwithpets")]
        public async Task<IActionResult> PetsWithUser([FromQuery] Guid petId)
        {
            if (petId == Guid.Empty)
            {
                return BadRequest("Guid inválido!");
            }

            var response = await this.userService.FindUsersByPet(petId);
            return Ok(response);
        }
    }
}
