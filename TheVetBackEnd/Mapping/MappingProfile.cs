﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheVet.Api.Resources;
using TheVetBackEnd.Models;

namespace TheVet.Api.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Resource to Domain
            CreateMap<SaveUserResource, User>();
            CreateMap<SavePetResource, Pet>().ForMember(pet => pet.PetUser, dto => dto.MapFrom(x => x.Users.Select(user => new UserPet() { UserCode = new Guid(user) })));
            CreateMap<PetResource, Pet>().ForMember(pet => pet.PetUser, dto => dto.MapFrom(x => x.Users.Select(user => new UserPet() { UserCode = new Guid(user), PetCode = x.Code })));

            // Domain to Resource
            CreateMap<User, UserResource>();
            CreateMap<Pet, SavePetResource>();
            CreateMap<Pet, PetResource>().ForMember(rscr => rscr.Users, pet => pet.MapFrom(data => data.PetUser.Select(petUser => petUser.UserCode.ToString().ToList())));

        }
    }
}
