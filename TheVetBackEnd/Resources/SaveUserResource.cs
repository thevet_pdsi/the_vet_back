﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TheVet.Api.Resources
{
    public class SaveUserResource
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string PassWord { get; set; }
        public string Username { get; set; }
        [Required]
        public string Cpf { get; set; }
    }
}
