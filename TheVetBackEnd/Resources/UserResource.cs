﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheVet.Api.Resources
{
    public class UserResource
    {
        public Guid Code { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
    }
}
