
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TheVetBackEnd.Models;

namespace TheVet.Api.Resources
{
    public class PetResource : SavePetResource
    {
        [Required]
        public Guid Code { get; set; }
    }
}
