using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TheVetBackEnd.Models;

namespace TheVet.Api.Resources
{
    public class SavePetResource
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime BirthDay { get; set; }
        [Required]
        public string Species { get; set; }
        [Required]
        public EnumGenre Gender { get; set; }
        [Required]
        public string PhysicalSape { get; set; }
        [Required]
        public string Breed { get; set; }
        [Required]
        public IEnumerable<string> Users { get; set; }
    }
}
