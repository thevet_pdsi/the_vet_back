using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TheVet.Core.Repositories;
using TheVetBackEnd.Models;

namespace TheVet.UnitTest.Services.Mock
{
    public class FakePetRepository : IPetRepository
    {
        public Pet Create(Pet entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Pet entity)
        {
            throw new NotImplementedException();
        }

        public bool Exists(Pet entity, Expression<Func<Pet, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Pet> FindAll()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Pet>> FindAllWithUser(Guid userId)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Pet> FindByCondition(Expression<Func<Pet, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public void Update(Pet entity)
        {
            throw new NotImplementedException();
        }
    }
}
