using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TheVet.Core.Contracts.Repositories;
using TheVet.Services.Utils;
using TheVetBackEnd.Models;

namespace TheVet.UnitTest.Services.Mock
{
    public class FakeUserRepository : IUserRepository
    {
        private IEnumerable<User> users;

        public FakeUserRepository()
        {
            this.users = new List<User>()
            {
                new User(){
                    Code = new Guid("d70b7e82-e96d-4f07-ba19-9bfc12e168da"),
                    Cpf = "111111111",
                    Email = "email@email.com",
                    Name = "User1",
                    PassWord = new CryptoPass("12345", new Guid("d70b7e82-e96d-4f07-ba19-9bfc12e168da")).HashPass(),
                    UserPet = new List<UserPet>(){
                        new UserPet(){
                            UserCode = new Guid("d70b7e82-e96d-4f07-ba19-9bfc12e168da"),
                            PetCode = new Guid("d8ee01e4-5973-4895-b558-99e6db9eac1b")
                        }
                    }
                },
                new User(){
                    Code = new Guid("b9a10e04-7ca3-494f-9a2c-025f21e45f06"),
                    Cpf = "22222222",
                    Email = "email2@email.com",
                    Name = "User 2",
                    PassWord =  new CryptoPass("54321", new Guid("b9a10e04-7ca3-494f-9a2c-025f21e45f06")).HashPass(), 
                    UserPet = new List<UserPet>()
                }
            };
        }
        public User Create(User entity)
        {
            this.users = this.users.Append(entity);
            return this.users.Last();
        }

        public void Delete(User entity)
        {
            throw new NotImplementedException();
        }

        public bool Exists(User entity, Expression<Func<User, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> FindAll()
        {
            throw new NotImplementedException();
        }

        public IQueryable<User> FindByCondition(Expression<Func<User, bool>> expression)
        {
            return this.users.AsQueryable().Where(expression);
        }

        public async Task<IEnumerable<User>> FindUsersByPetId(Guid petId)
        {
            await Task.Delay(1000);
            return this.users.Where(user => user.UserPet.Where(userPet => userPet.PetCode == petId).Any());
        }

        public void Update(User entity)
        {
            throw new NotImplementedException();
        }
    }
}