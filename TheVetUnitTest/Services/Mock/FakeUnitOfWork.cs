using System.Threading.Tasks;
using TheVet.Core.Contracts;
using TheVet.Core.Contracts.Repositories;
using TheVet.Core.Repositories;

namespace TheVet.UnitTest.Services.Mock
{

    public class FakeUnitOfWork : IUnitOfWork
    {
        private IPetRepository fakePetRepo;
        private IUserRepository fakeUserRepo;
        public IPetRepository PetRepository
        {
            get
            {
                if (this.fakePetRepo == null)
                {
                    this.fakePetRepo = new FakePetRepository();
                }
                return this.fakePetRepo;
            }
        }

        public IUserRepository UserRepository
        {
            get
            {
                if (this.fakeUserRepo == null)
                {
                    this.fakeUserRepo = new FakeUserRepository();
                }
                return this.fakeUserRepo;
            }
        }

        public async Task<int> Commit()
        {
            await Task.Delay(10000);
            return 1;
        }
    }

}