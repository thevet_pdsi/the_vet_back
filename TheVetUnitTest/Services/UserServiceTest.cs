using System;
using System.Linq;
using TheVet.Core.Contracts;
using TheVet.Core.Contracts.Services;
using TheVet.Core.Utils;
using TheVet.Services.Services;
using TheVet.Services.Utils;
using TheVet.UnitTest.Services.Mock;
using TheVetBackEnd.Models;
using Xunit;

namespace TheVet.UnitTest.Services
{
    public class UserServiceTest
    {
        private IUnitOfWork unitOfWork;
        private IUserService userService;

        public UserServiceTest()
        {
            this.unitOfWork = new FakeUnitOfWork();
            this.userService = new UserService(this.unitOfWork);
        }

        [Fact]
        public async void CreateUserSuccessful()
        {
            var arrange = new User()
            {
                Name = "TesteCreate",
                Cpf = "0000000",
                Email = "emailCreate@email.com",
                PassWord = "12345",
            };

            var result = await this.userService.CreateUser(arrange);
            Assert.IsType<User>(result);
            Assert.NotEqual(Guid.Empty, result.Code);
            Assert.Equal(arrange.Name, result.Name);
        }


        //TODO: Fazer todos os caminhos alternativos
        [Fact]
        public void ValidateLoginSuccesful()
        {
            // user already defined at FakeUserRepository
            var arrangeUsername = "email@email.com";
            var arrangepassword = "12345";

            var result = this.userService.ValidateLogin(arrangeUsername, arrangepassword);

            var expectedCode = new Guid("d70b7e82-e96d-4f07-ba19-9bfc12e168da");
            Assert.IsType<AuthenticationResult>(result);
            Assert.Equal(true, result.Success);
            Assert.Equal(expectedCode, result.ReturnedUser.Code);
        }

        [Fact]
        public async void GetUsersByPetSuccesful()
        {
            var arrangePet = new Guid("d8ee01e4-5973-4895-b558-99e6db9eac1b");
            var result = await this.userService.FindUsersByPet(arrangePet);
            Assert.Equal(1, result.Count());
        }
    }
}