using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using TheVet.Api.Controllers;
using TheVet.Api.Resources;
using TheVet.Core.Contracts.Services;
using TheVet.Core.Utils;
using TheVetBackEnd.Models;
using Xunit;

namespace TheVet.UnitTest.Controllers
{
    public class UserControllerTest
    {
        private UserController userController;
        private User user;
        private UserResource userResource;
        private AuthenticationResult authPositive;
        private SaveUserResource saveUser;

        private Guid guidPet;

        public UserControllerTest()
        {
            this.ConfigureMock();
        }

        private void ConfigureMock()
        {
            var mapper = AutomapperSingleton.Mapper;
            var serviceMock = new Mock<IUserService>();

            this.guidPet = Guid.NewGuid();
            var userCode = Guid.NewGuid();
            this.user = new User()
            {
                Code = userCode,
                Cpf = "12345678",
                Email = "email@email.com",
                Name = "Josephus",
                PassWord = "12345",
                Username = "joseP",
                UserPet = new List<UserPet>() {
                    new UserPet (){
                        PetCode = this.guidPet,
                        UserCode = userCode
                    }
                }
            };

            this.saveUser = new SaveUserResource()
            {
                Cpf = this.user.Cpf,
                Email = this.user.Email,
                Name = this.user.Name,
                PassWord = this.user.PassWord,
            };

            this.userResource = new UserResource()
            {
                Code = userCode,
                Email = this.user.Email,
                Name = this.user.Name,
                Username = this.user.Username
            };

            this.authPositive = new AuthenticationResult()
            {
                Message = string.Empty,
                Success = true,
                ReturnedUser = this.user
            };

            serviceMock.Setup(us => us.CreateUser(It.IsAny<User>())).ReturnsAsync(this.user);
            serviceMock.Setup(us => us.ValidateLogin(this.user.Email, this.user.PassWord)).Returns(this.authPositive);
            serviceMock.Setup(us => us.FindUsersByPet(this.guidPet)).ReturnsAsync(new List<User>() { this.user });

            this.userController = new UserController(serviceMock.Object, mapper);
        }

        [Fact]
        public async void CreateUserSuccessful()
        {
            //TODO: Verificar se o retorno com usuário é utilizado no front
            //TODO: Pq está dando erro aqui??
            var arrangeUser = this.saveUser;
            var result = await this.userController.PostUser(arrangeUser);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void CreateUserBadRequest()
        {
            var arrangeUser = new SaveUserResource()
            {
                Cpf = "12345",
                Email = "email@email.com",
                Name = "Joaozinho"
            };

            this.userController.ModelState.AddModelError("Password", "Required");

            var result = await this.userController.PostUser(arrangeUser);
            Assert.IsType<BadRequestObjectResult>(result);
        }


        [Fact]
        public void LoginUserSuccessful()
        {
            var arrangeLogin = new UserLogin()
            {
                Username = this.user.Email,
                Password = this.user.PassWord
            };

            var result = this.userController.Login(arrangeLogin);
            var okResult = result as OkObjectResult;

            Assert.Equal(this.userResource.Code, ((UserResource)okResult.Value).Code);
        }

        [Fact]
        public async void GetUsersByPetsSuccessful()
        {
            var arrangePetId = this.user.UserPet.Select(x => x.PetCode).First();
            var result = await this.userController.PetsWithUser(arrangePetId);

            var items = result as OkObjectResult;
            Assert.Equal(1, ((IEnumerable<User>)items.Value).Count());
        }

        [Fact]
        public async void GetUserByPetsEmptyGuid()
        {
            var arrangeId = Guid.Empty;
            var result = await this.userController.PetsWithUser(arrangeId);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void GetUserByPetsWrongGuid()
        {
            var arrangeId = Guid.NewGuid();
            var result = await this.userController.PetsWithUser(arrangeId);
            var items = result as OkObjectResult;
            Assert.Equal(0, ((IEnumerable<User>)items.Value).Count());
        }
    }
}
