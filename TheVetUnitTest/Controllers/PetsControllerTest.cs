using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Moq;
using TheVet.Api.Resources;
using TheVet.Core.Contracts.Services;
using TheVetBackEnd.Controllers;
using TheVetBackEnd.Models;
using Xunit;

namespace TheVet.UnitTest.Controllers
{

    public class PetsControllerTest
    {
        private Pet createdPet;
        private PetsController controller;
        private SavePetResource savePetResource;
        private PetResource petResource;
        private Guid userCode;
        public PetsControllerTest()
        {
            this.ConfigureMock();
        }

        private void ConfigureMock()
        {
            var serviceMock = new Mock<IPetService>();
            userCode = Guid.NewGuid();
            var petCode = Guid.NewGuid();

            var owner = new User()
            {
                Code = userCode,
                Email = "Email teste",
                Cpf = "123456789",
                Name = "Joãozinho",
                PassWord = "12345",
            };

            this.createdPet = new Pet()
            {
                Code = petCode,
                BirthDay = DateTime.Now,
                Gender = EnumGenre.Male,
                Breed = "Pastor Alemão",
                Name = "Doguinho",
                PhysicalSape = "Grande Porte",
                Species = "Cachorro",
                PetUser = new List<UserPet>() { new UserPet() { PetCode = petCode, UserCode = userCode } }
            };

            this.savePetResource = new SavePetResource()
            {
                Breed = this.createdPet.Breed,
                BirthDay = this.createdPet.BirthDay.Value,
                Gender = this.createdPet.Gender,
                PhysicalSape = this.createdPet.PhysicalSape,
                Name = this.createdPet.Name,
                Species = this.createdPet.Species,
                Users = this.createdPet.PetUser.Select(x => x.UserCode.ToString())
            };

            this.petResource = new PetResource()
            {
                Code = this.createdPet.Code,
                Breed = this.createdPet.Breed,
                BirthDay = this.createdPet.BirthDay.Value,
                Gender = this.createdPet.Gender,
                PhysicalSape = this.createdPet.PhysicalSape,
                Name = this.createdPet.Name,
                Species = this.createdPet.Species,
                Users = this.createdPet.PetUser.Select(x => x.UserCode.ToString())
            };

            serviceMock.Setup(pService => pService.CreatePet(It.IsAny<Pet>())).ReturnsAsync(1);
            serviceMock.Setup(pService => pService.FindAllWithUser(userCode)).ReturnsAsync(new List<Pet>() { this.createdPet });
            serviceMock.Setup(pService => pService.UpdatePet(createdPet)).ReturnsAsync(1);
            serviceMock.Setup(pService => pService.Delete(createdPet.Code)).ReturnsAsync(1);

            var mapper = AutomapperSingleton.Mapper;
            this.controller = new PetsController(serviceMock.Object, mapper);
        }


        [Fact]
        public async void CreatePetSuccessful()
        {
            var result = await this.controller.PostPet(this.savePetResource);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void CreatePetBadRequest()
        {
            SavePetResource arrangePet = null;
            var result = await this.controller.PostPet(arrangePet);

            Assert.NotNull(result);
            Assert.IsType<BadRequestObjectResult>(result);
        }


        [Fact]
        public async void FindPetsByUserSuccessful()
        {
            var arragenUserId = this.createdPet.PetUser.Select(x => x.UserCode).First();

            var result = await this.controller.PetsWithUser(arragenUserId);

            Assert.IsType<OkObjectResult>(result);

            var items = result as OkObjectResult;

            Assert.Equal(1, ((IEnumerable<Pet>)items.Value).Count());
        }


        [Fact]
        public async void UpdatePetSuccessful()
        {

            var result = await this.controller.PutPet(this.createdPet.Code, this.petResource);

            var expectedValue = "Pet atualizado com sucesso!";

            Assert.Equal(expectedValue, ((OkObjectResult)result).Value);
        }
    
        [Fact]
        public async void UpdatPetBadRequest()
        {
            var result = await this.controller.PutPet(Guid.NewGuid(), this.petResource);

            var expectedValue = "O código do Pet selecionado é diferente do parâmetro enviado na rota!" ;

            Assert.Equal(expectedValue, ((BadRequestObjectResult)result).Value);
        }
    }
}