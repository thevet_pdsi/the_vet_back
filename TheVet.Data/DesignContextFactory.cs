using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace TheVet.Data
{
public class BloggingContextFactory : Microsoft.EntityFrameworkCore.Design.IDesignTimeDbContextFactory<DatabaseContext>
    {
        public DatabaseContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DatabaseContext>();
            optionsBuilder.UseMySql("Server=localhost;User Id=nicoolas;Password=12345;Database=thevetmigration; pooling = false;convert zero datetime=True");

            return new DatabaseContext(optionsBuilder.Options);
        }
    }
}