﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TheVet.Core.Contracts.Repositories;
using TheVetBackEnd.Models;

namespace TheVet.Data.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(DatabaseContext dbContext) : base(dbContext) { }

        public async Task<IEnumerable<User>> FindUsersByPetId(Guid petId)
        {
            return await this.RepositoryContext.Set<User>().Where(user => user.UserPet.Where(userPet => userPet.PetCode == petId).Any()).Include(userModel => userModel.UserPet).ToListAsync();
        }
    }
}
