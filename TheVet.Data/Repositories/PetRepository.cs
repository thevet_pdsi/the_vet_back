﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TheVet.Core.Repositories;
using TheVetBackEnd.Models;

namespace TheVet.Data.Repositories
{
    public class PetRepository : RepositoryBase<Pet>, IPetRepository
    {
        public PetRepository(DatabaseContext dbContext) : base(dbContext) { }

        public async Task<IEnumerable<Pet>> FindAllWithUser(Guid userId)
        {
            return await this.RepositoryContext.Set<Pet>().Where(pet => pet.PetUser.Where(petUser => petUser.UserCode == userId).Any()).Include(petModel => petModel.PetUser).ToListAsync();
        }
    }
}