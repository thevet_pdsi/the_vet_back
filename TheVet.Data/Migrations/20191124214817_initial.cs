﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TheVet.Data.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Pet",
                columns: table => new
                {
                    Code = table.Column<Guid>(type: "char(36)", nullable: false),
                    BirthDay = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Breed = table.Column<string>(type: "longtext", nullable: true),
                    Gender = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "longtext", nullable: true),
                    PhysicalSape = table.Column<string>(type: "longtext", nullable: true),
                    Species = table.Column<string>(type: "longtext", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pet", x => x.Code);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Code = table.Column<Guid>(type: "char(36)", nullable: false),
                    Cpf = table.Column<string>(type: "longtext", nullable: true),
                    Email = table.Column<string>(type: "longtext", nullable: true),
                    Name = table.Column<string>(type: "longtext", nullable: true),
                    PassWord = table.Column<string>(type: "longtext", nullable: true),
                    Username = table.Column<string>(type: "longtext", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Code);
                });

            migrationBuilder.CreateTable(
                name: "UserPet",
                columns: table => new
                {
                    UserCode = table.Column<Guid>(type: "char(36)", nullable: false),
                    PetCode = table.Column<Guid>(type: "char(36)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPet", x => new { x.UserCode, x.PetCode });
                    table.ForeignKey(
                        name: "FK_UserPet_Pet_PetCode",
                        column: x => x.PetCode,
                        principalTable: "Pet",
                        principalColumn: "Code",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserPet_User_UserCode",
                        column: x => x.UserCode,
                        principalTable: "User",
                        principalColumn: "Code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserPet_PetCode",
                table: "UserPet",
                column: "PetCode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserPet");

            migrationBuilder.DropTable(
                name: "Pet");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
