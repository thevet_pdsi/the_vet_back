﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheVet.Core.Contracts;
using TheVet.Core.Contracts.Repositories;
using TheVet.Core.Repositories;
using TheVet.Data.Repositories;

namespace TheVet.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private DatabaseContext databaseContext;

        private IPetRepository petRepository;

        private IUserRepository userRepository;

        public UnitOfWork(DatabaseContext repositoryContext)
        {
            databaseContext = repositoryContext;
        }

        public IPetRepository PetRepository
        {
            get
            {
                if (this.petRepository == null)
                {
                    this.petRepository = new PetRepository(databaseContext);
                }
                return this.petRepository;
            }
        }

        public IUserRepository UserRepository
        {
            get
            {
                if (this.userRepository == null)
                {
                    this.userRepository = new UserRepository(databaseContext);
                }
                return this.userRepository;
            }
        }


        public Task<int> Commit()
        {
            return this.databaseContext.SaveChangesAsync();
        }
    }
}
