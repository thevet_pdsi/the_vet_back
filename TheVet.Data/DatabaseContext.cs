﻿using Microsoft.EntityFrameworkCore;
using TheVetBackEnd.Models;

namespace TheVet.Data
{
    public class DatabaseContext : DbContext
    {

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Pet>().ToTable("Pet");
            modelBuilder.Entity<UserPet>().HasKey(sc => new { sc.UserCode, sc.PetCode });
        }

        public DbSet<User> User { get; set; }
        public DbSet<Pet> Pet { get; set; }
    }
}
