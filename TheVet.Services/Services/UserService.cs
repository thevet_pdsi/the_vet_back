﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheVet.Core.Contracts;
using TheVet.Core.Contracts.Services;
using TheVet.Core.Utils;
using TheVet.Services.Utils;
using TheVetBackEnd.Models;

namespace TheVet.Services.Services
{
    public class UserService : IUserService
    {
        private IUnitOfWork unitOfWork;
        public UserService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<User> CreateUser(User user)
        {
            user.Code = Guid.NewGuid();
            user.PassWord = new CryptoPass(user.PassWord, user.Code).HashPass();
            var creteadUser = this.unitOfWork.UserRepository.Create(user);
            await this.unitOfWork.Commit();
            return creteadUser;
        }

        public AuthenticationResult ValidateLogin(string username, string password)
        {
            var returnedUser = this.unitOfWork.UserRepository.FindByCondition(user => user.Username == username || user.Email == username).FirstOrDefault();
            if (returnedUser == null)
            {
                return new AuthenticationResult()
                {
                    Success = false,
                    Message = "O usuário não existe no banco de dados"
                };
            }
            
            var hashedPass = new CryptoPass(password, returnedUser.Code).HashPass();

            if (returnedUser.PassWord != hashedPass)
            {
                return new AuthenticationResult()
                {
                    Success = false,
                    Message = "Senha incorreta"
                };
            }
            else
            {
                return new AuthenticationResult()
                {
                    Success = true,
                    ReturnedUser = returnedUser
                };
            }
        }

        public async Task<IEnumerable<User>> FindUsersByPet(Guid petId)
        {
            return await this.unitOfWork.UserRepository.FindUsersByPetId(petId);
        }
    }
}
