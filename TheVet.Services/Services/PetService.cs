﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheVet.Core.Contracts;
using TheVet.Core.Contracts.Services;
using TheVetBackEnd.Models;

namespace TheVet.Services.Services
{
    public class PetService : IPetService
    {
        IUnitOfWork unitOfWork;

        public PetService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> CreatePet(Pet pet)
        {
            pet.Code = Guid.NewGuid();
            pet.PetUser = pet.PetUser.Select(uPet => new UserPet() { UserCode = uPet.UserCode, PetCode = uPet.PetCode == Guid.Empty ? pet.Code : uPet.PetCode }).ToList();
            this.unitOfWork.PetRepository.Create(pet);
            return await this.unitOfWork.Commit();
        }

        public async Task<int> Delete(Guid id)
        {
            var selectedPet = this.unitOfWork.PetRepository.FindByCondition(pet => pet.Code == id).FirstOrDefault();
            this.unitOfWork.PetRepository.Delete(selectedPet);
            return await this.unitOfWork.Commit();
        }

        public IEnumerable<Pet> FindAllPets()
        {
            return this.unitOfWork.PetRepository.FindAll();
        }

        public Pet FindPetById(Guid id)
        {
            return this.unitOfWork.PetRepository.FindByCondition(user => user.Code == id).FirstOrDefault();
        }

        public async Task<int> UpdatePet(Pet pet)
        {
            this.unitOfWork.PetRepository.Update(pet);
            return await this.unitOfWork.Commit();
        }

        public async Task<IEnumerable<Pet>> FindAllWithUser(Guid userId)
        {
            return await this.unitOfWork.PetRepository.FindAllWithUser(userId);
        }
    }
}
