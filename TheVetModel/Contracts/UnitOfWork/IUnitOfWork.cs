﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheVet.Core.Contracts.Repositories;
using TheVet.Core.Repositories;

namespace TheVet.Core.Contracts
{
    public interface IUnitOfWork
    {
        IPetRepository PetRepository { get; }
        IUserRepository UserRepository { get; }

        Task<int> Commit();
    }
}
