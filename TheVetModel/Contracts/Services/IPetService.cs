﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheVetBackEnd.Models;

namespace TheVet.Core.Contracts.Services
{
    public interface IPetService
    {
        IEnumerable<Pet> FindAllPets();
        Pet FindPetById(Guid id);
        Task<int> UpdatePet(Pet pet);
        Task<int> CreatePet(Pet pet);
        Task<int> Delete(Guid id);  
        Task<IEnumerable<Pet>> FindAllWithUser(Guid userId);

    }
}
