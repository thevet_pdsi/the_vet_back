﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheVet.Core.Utils;
using TheVetBackEnd.Models;

namespace TheVet.Core.Contracts.Services
{
    public interface IUserService
    {
        Task<User> CreateUser(User user);
        AuthenticationResult ValidateLogin(string username, string password);
        Task<IEnumerable<User>> FindUsersByPet(Guid petId);
    }
}
