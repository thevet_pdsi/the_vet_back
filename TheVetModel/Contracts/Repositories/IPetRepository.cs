﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TheVetBackEnd.Models;

namespace TheVet.Core.Repositories
{
    public interface IPetRepository : IRepositoryBase<Pet>
    {
        Task<IEnumerable<Pet>> FindAllWithUser(Guid userId);
    }
}
