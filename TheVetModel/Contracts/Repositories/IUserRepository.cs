﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheVet.Core.Repositories;
using TheVetBackEnd.Models;

namespace TheVet.Core.Contracts.Repositories
{
    public interface IUserRepository : IRepositoryBase<User>
    {
        Task<IEnumerable<User>> FindUsersByPetId(Guid petId);
    }
}
