﻿using System;
using System.Collections.Generic;
using System.Text;
using TheVetBackEnd.Models;

namespace TheVet.Core.Utils
{
    public class AuthenticationResult
    {
        public bool Success { get; set; }

        public string Message { get; set; }

        public User ReturnedUser { get; set; }
    }
}
