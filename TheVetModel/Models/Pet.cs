﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TheVetBackEnd.Models
{
    public class Pet
    {
        [Key]
        public Guid Code { get; set; }
        public string Name { get; set; }
        public DateTime? BirthDay { get; set; }
        public string Species { get; set; } 
        public EnumGenre Gender { get; set; }
        public string PhysicalSape { get; set; }
        public string Breed { get; set; }
        public virtual IList<UserPet> PetUser { get; set; }
    }
}
