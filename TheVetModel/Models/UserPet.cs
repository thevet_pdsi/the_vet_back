﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheVetBackEnd.Models
{
    public class UserPet
    {
        public Guid PetCode { get; set; }
        public Pet Pet { get; set; }

        public Guid UserCode { get; set; }
        public User User { get; set; }
    }
}
