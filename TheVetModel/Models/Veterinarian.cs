﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheVetBackEnd.Models
{
    public class Veterinarian : User
    {
        public string Crmv { get; set; }
        public string Especialidade { get; set; }
    }
}
