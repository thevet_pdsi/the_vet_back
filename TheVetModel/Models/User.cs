﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TheVetBackEnd.Models
{
    public class User
    {
        [Key]
        public Guid Code { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string PassWord { get; set; }
        public string Username { get; set; }
        public string Cpf { get; set; }
        public IList<UserPet> UserPet { get; set; }
    }
}
